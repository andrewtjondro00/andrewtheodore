from django import forms

class EntryForm(forms.Form):
    attrs = { 
        'class': 'form-control' 
    } 

    message = forms.CharField(label="",max_length=300,widget = forms.Textarea(attrs = attrs))
